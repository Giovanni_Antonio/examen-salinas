package balam.app.examensalinas.controller.model;

public class CalificacionesModel {
    private String Matricula, Nombre, Grado, Promedio, PromedioGeneral;

    public CalificacionesModel(String matricula, String nombre, String grado, String promedio, String promedioGeneral) {
        Matricula = matricula;
        Nombre = nombre;
        Grado = grado;
        Promedio = promedio;
        PromedioGeneral = promedioGeneral;
    }

    public CalificacionesModel() {
    }

    public String getMatricula() {
        return Matricula;
    }

    public void setMatricula(String matricula) {
        Matricula = matricula;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getGrado() {
        return Grado;
    }

    public void setGrado(String grado) {
        Grado = grado;
    }

    public String getPromedio() {
        return Promedio;
    }

    public void setPromedio(String promedio) {
        Promedio = promedio;
    }

    public String getPromedioGeneral() {
        return PromedioGeneral;
    }

    public void setPromedioGeneral(String promedioGeneral) {
        PromedioGeneral = promedioGeneral;
    }
}
