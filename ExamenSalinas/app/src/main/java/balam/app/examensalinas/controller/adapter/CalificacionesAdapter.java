package balam.app.examensalinas.controller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import balam.app.examensalinas.R;
import balam.app.examensalinas.controller.model.CalificacionesModel;
import balam.app.examensalinas.controller.ui.activitis.MostrarCalificaciones;

public class CalificacionesAdapter extends RecyclerView.Adapter<CalificacionesAdapter.calificacionView>implements Filterable {

    private List<CalificacionesModel> calificacionesList = new ArrayList<>();
    private Context context;

    private ArrayList<CalificacionesModel> personaArrayList;



    public CalificacionesAdapter(MostrarCalificaciones mostrarCalificaciones, ArrayList<CalificacionesModel> personaList) {
        this.calificacionesList = personaList;
        this.personaArrayList = personaList;
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String palabra = constraint.toString();

                if (palabra.isEmpty()) {
                    calificacionesList = personaArrayList;
                } else {
                    ArrayList<CalificacionesModel> filtrarLista = new ArrayList<>();
                    for (CalificacionesModel calificacionesModel : personaArrayList) {
                        if (calificacionesModel.getGrado().toLowerCase().contains(constraint)) {
                            filtrarLista.add(calificacionesModel);
                        }
                    }
                    personaArrayList = filtrarLista;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = personaArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                personaArrayList = (ArrayList<CalificacionesModel>) results.values;
                notifyDataSetChanged();
            }
        };

    }

    @NonNull
    @Override
    public calificacionView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calificaciones, parent, false);
        return new calificacionView(view);
    }

    @Override
    public void onBindViewHolder(calificacionView holder, int position) {
        CalificacionesModel persona = calificacionesList.get(position);
        int reproprobado = Integer.parseInt(persona.getPromedio());
        holder.personName.setText("Nombre:\n"+" "+(String.valueOf(persona.getMatricula())));
        holder.personGrupo.setText("Grado:\n"+" "+(persona.getNombre()+"°"));
        holder.personMatricula.setText("Matricula:\n"+" "+(persona.getGrado()));
        if (reproprobado<=5){
            holder.personPromedio.setTextColor(Color.RED);
            holder.personPromedio.setText(("Reprobado:\n"+" "+persona.getPromedio()));
        }else {
            holder.personPromedio.setText(("Promedio:\n"+" "+persona.getPromedio()));
        }
        holder.promedioGrupo.setText(("Promedio del grupo:\n"+" "+persona.getPromedioGeneral()));

    }



    @Override
    public int getItemCount() {
        return calificacionesList.size();
    }
    public void agregarPersona(CalificacionesModel calificacionesModel) {
        calificacionesList.add(calificacionesModel);
        this.notifyDataSetChanged();
    }
    public class calificacionView extends RecyclerView.ViewHolder {
        private TextView personName, personGrupo, personMatricula, personPromedio,promedioGrupo;

        public calificacionView(@NonNull View itemView) {
            super(itemView);
            personName = itemView.findViewById(R.id.person_name);
            personGrupo = itemView.findViewById(R.id.person_grupo);
            personMatricula = itemView.findViewById(R.id.person_matricula);
            personPromedio = itemView.findViewById(R.id.person_promedio);
            promedioGrupo = itemView.findViewById(R.id.promedio_grupo);
        }
    }


}
