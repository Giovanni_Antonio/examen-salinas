package balam.app.examensalinas.controller.ui.activitis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import java.util.ArrayList;

import balam.app.examensalinas.R;
import balam.app.examensalinas.controller.adapter.CalificacionesAdapter;
import balam.app.examensalinas.controller.model.CalificacionesModel;
import balam.app.examensalinas.database.CalificacionesBaseDatos;

public class MostrarCalificaciones extends AppCompatActivity {

    RecyclerView idrecyclerview;
    ArrayList<CalificacionesModel> personaArrayList;
    CalificacionesBaseDatos sqlLite;
    private CalificacionesAdapter calificacionesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_calificaciones);

        idrecyclerview = findViewById(R.id.idrecyclerview);
        personaArrayList = new ArrayList<>();
        sqlLite = new CalificacionesBaseDatos(this, "persona", null, 1);
        calificacionesAdapter = new CalificacionesAdapter(this, personaArrayList);

        RecyclerView recyclerView = findViewById(R.id.idrecyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(calificacionesAdapter);
        mostrarDatos();

    }

    public void mostrarDatos() {
        SQLiteDatabase sqLiteDatabase = sqlLite.getReadableDatabase();
        CalificacionesModel calificaciones = null;
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM persona", null);
        while (cursor.moveToNext()) {
            calificaciones = new CalificacionesModel();
            calificaciones.setMatricula(cursor.getString(0));
            calificaciones.setNombre(cursor.getString(1));
            calificaciones.setGrado(cursor.getString(2));
            calificaciones.setPromedio(cursor.getString(3));
            calificacionesAdapter.agregarPersona(calificaciones);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_buscar,menu);
        MenuItem buscars=menu.findItem(R.id.idbuscar);
        SearchView searchView=(SearchView) MenuItemCompat.getActionView(buscars);
        buscar(searchView);
        return true;

    }

    private void buscar(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(calificacionesAdapter!=null)
                    calificacionesAdapter.getFilter().filter((newText));
                return true;
            }
        });
    }
}