package balam.app.examensalinas.controller.ui.activitis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import org.w3c.dom.Text;

import balam.app.examensalinas.R;
import balam.app.examensalinas.database.CalificacionesBaseDatos;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.edt_matricula)
    protected TextInputEditText edtMatricula;
    @BindView(R.id.edt_name)
    protected TextInputEditText edtName;
    @BindView(R.id.edt_materia_uno)
    protected TextInputEditText edtMateriaUno;
    @BindView(R.id.edt_materia_dos)
    protected TextInputEditText edtMateriaDos;
    @BindView(R.id.edt_materia_tres)
    protected TextInputEditText edtMateriaTres;
    @BindView(R.id.rbtn_uno)
    protected RadioButton radioUno;
    @BindView(R.id.rbtn_dos)
    protected RadioButton radioDos;
    @BindView(R.id.rbtn_tres)
    protected RadioButton radioTres;
    @BindView(R.id.rbtn_cuatro)
    protected RadioButton radioCuatro;
    @BindView(R.id.rbtn_cinco)
    protected RadioButton radioCinco;
    @BindView(R.id.rbtn_seis)
    protected RadioButton radioSeis;
    protected Button btnSalvar;
    protected Button btnConsultar;
    int grado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        salvar();
        consultar();
    }

    private void init() {
        ButterKnife.bind(this);
        btnSalvar = findViewById(R.id.btn_guardar);
        btnConsultar = findViewById(R.id.btn_consultar);

    }

    @OnCheckedChanged({R.id.rbtn_uno, R.id.rbtn_dos, R.id.rbtn_tres, R.id.rbtn_cuatro, R.id.rbtn_cinco, R.id.rbtn_seis })
    public void onRadioButtonClicked(CompoundButton button, boolean checked) {
        if (checked){
            switch (button.getId()){
                case R.id.rbtn_uno:
                    grado = 1;
                   edtMateriaUno.setHint(R.string.espanol);
                   edtMateriaDos.setHint(R.string.matematicas);
                    edtMateriaTres.setVisibility(View.GONE);
                    break;
                case R.id.rbtn_dos:
                    grado = 2;
                    edtMateriaUno.setHint(R.string.c_sociales);
                    edtMateriaDos.setHint(R.string.espanol);
                    edtMateriaTres.setVisibility(View.GONE);
                    break;
                case R.id.rbtn_tres:
                    grado = 3;
                    edtMateriaUno.setHint(R.string.espanol);
                    edtMateriaDos.setHint(R.string.matematicas);
                    edtMateriaTres.setHint(R.string.civismo);
                    edtMateriaTres.setVisibility(View.VISIBLE);
                    break;
                case R.id.rbtn_cuatro:
                    grado = 4;
                    edtMateriaUno.setHint(R.string.espanol);
                    edtMateriaDos.setHint(R.string.matematicas);
                    edtMateriaTres.setHint(R.string.e_fisica);
                    edtMateriaTres.setVisibility(View.VISIBLE);
                    break;
                case R.id.rbtn_cinco:
                    grado = 5;
                    edtMateriaUno.setHint(R.string.espanol);
                    edtMateriaDos.setHint(R.string.quimica);
                    edtMateriaTres.setHint(R.string.fisica);
                    edtMateriaTres.setVisibility(View.VISIBLE);
                    break;
                case R.id.rbtn_seis:
                    grado = 6;
                    edtMateriaUno.setHint(R.string.espanol);
                    edtMateriaDos.setHint(R.string.matematicas);
                    edtMateriaTres.setHint(R.string.fisica);
                    edtMateriaTres.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    }

    private void consultar(){
        btnConsultar.setOnClickListener(view -> {
            Intent intent=new Intent(MainActivity.this,MostrarCalificaciones.class);
            startActivity(intent);
        });

    }
    private void salvar(){
        btnSalvar.setOnClickListener(view -> {
            if(edtMatricula.getText().toString().equals("")|| edtName.getText().toString().equals("")){
                validarTextos();
            }else{
                GuardarDatos(view);
                limpiarTextos();
                Toast.makeText(MainActivity.this, "Datos Registrados", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void limpiarTextos() {
        edtName.setText("");
        edtMatricula.setText("");
        edtMateriaUno.setText("");
        edtMateriaDos.setText("");
        edtMateriaTres.setText("");

    }

    public void validarTextos() {
        if (edtMatricula.getText().toString().equals("")) {
            edtMatricula.setText("Falta el codigo");
        }
        if (edtName.getText().toString().equals("")) {
            edtName.setText("Falta el Nombre");
        }
    }

    private void GuardarDatos(View v) {

        CalificacionesBaseDatos sqlLite = new CalificacionesBaseDatos(this, "persona", null, 1);
        SQLiteDatabase sqLiteDatabase = sqlLite.getWritableDatabase();

        String matricula = edtMatricula.getText().toString();
        String nombre = edtName.getText().toString();

        int promedio;
        Integer tres;
        Integer uno = Integer.parseInt(edtMateriaUno.getText().toString());
        Integer dos = Integer.parseInt(edtMateriaDos.getText().toString());
        if (!edtMateriaTres.getText().toString().isEmpty() ){
            tres = Integer.parseInt(edtMateriaTres.getText().toString());
            int suma = uno+dos+tres;
            promedio = suma/3;
        }else {
            int suma = uno+dos;
            promedio = suma/2;
        }
        String resultado = Integer.toString(promedio);
        String gradoCursando = Integer.toHexString(grado);

        ContentValues values = new ContentValues();
        values.put("matricula", matricula);
        values.put("nombre", nombre);
        values.put("resultado", resultado);
        values.put("grado",gradoCursando);


        Long respuesta = sqLiteDatabase.insert("persona", null, values);
        Toast.makeText(this, "Resultado: " + respuesta, Toast.LENGTH_SHORT).show();
    }


}